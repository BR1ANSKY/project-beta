import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerForm from './CustomerForm';
import RecordSaleForm from './RecordSaleForm';
import ListSalesPeople from './ListSalesPeople';
import ListAllSales from './ListAllSales';
import ListCustomers from './ListCustomers';
import SalesPersonForm from './SalesPersonForm';
import ManufacturersList from './ManufacturesList';
import AutomobileList from './AutomobileList';
import AutomobileForm from './CreateAutomobileForm';
import ManufacturerForm from './CreateManufacturerForm';
import VehicleModelForm from './CreateVehicleModelForm';
import ModelsList from './ModelList';
import AppointmentForm from './Service/AppointmentForm'
import AppointmentList from './Service/ListServiceAppointments'
import AppointmentHistoryList from './Service/ListServiceHistory'
import ListTechnicians from './Service/ListTechnicians'
import TechnicianForm from './Service/TechnicianForm'

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="customers/list/" element={<ListCustomers />} />
          <Route path="customers/" element={<CustomerForm />} />
          <Route path="salespeople/list/" element={<ListSalesPeople />} />
          <Route path="salespeople/" element={<SalesPersonForm />} />
          <Route path="sales/" element={<RecordSaleForm />} />
          <Route path="sales/history/" element={<ListAllSales />} />
          <Route path="models/create/" element={<VehicleModelForm />} />
          <Route path="manufacturers/create/" element={<ManufacturerForm />} />
          <Route path="automobiles/create/" element={<AutomobileForm />} />
          <Route path="manufacturers/list/" element={<ManufacturersList />}/>
          <Route path="automobiles/list/" element={<AutomobileList />}/>
          <Route path="models/list/" element={<ModelsList />}/>
          <Route path="technicians/create/" element={<TechnicianForm />} />
          <Route path="technicians/" element={<ListTechnicians />} />
          <Route path="appointments/create/" element={<AppointmentForm />} />
          <Route path="appointments/" element={<AppointmentList />}/>
          <Route path="appointments/history/" element={<AppointmentHistoryList />}/>





        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
