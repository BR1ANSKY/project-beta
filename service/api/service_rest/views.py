from .models import Appointment, Technician, AutomobileVO
from .encoders import AutomobileEncoder, TechnicianListEncoder, AppointmentListEncoder
from rest_framework.views import APIView
from rest_framework.response import Response
import json
# Create your views here.


class AppointmentViews(APIView):
    def get(self, request):
        appointments = Appointment.objects.all()
        json_data = AppointmentListEncoder(appointments, many=True)
        return Response(json_data)
    
    def post(self, request):
        json_data = request.data
        appointment = Appointment.objects.create(**json_data)
        data = AppointmentListEncoder(appointment).data
        return Response(data)
    
    def delete(self, request, pk):
        Appointment.objects.get(pk=pk).delete()
        return Response()


class TechnicianViews(APIView):
    def get(self, request):
        technicians = Technician.objects.all()
        json_data = TechnicianListEncoder(technicians, many=True)
        return Response(json_data)
    
    def post(self, request):
        json_data = request.data
        technician = Technician.objects.create(**json_data)
        data = TechnicianListEncoder(technician).data
        return Response(data)
    
    def delete(self, request, pk):
        Technician.objects.get(pk=pk).delete()
        return Response()


class AutomobileVOViews(APIView):
    def get(self):
        automobiles = AutomobileVO.objects.all()
        json_data = AutomobileEncoder(automobiles, many=True)
        return Response(json_data)
    
    def post(self, request):
        json_data = request.data
        automobile = AutomobileVO.objects.create(**json_data)
        data = AutomobileEncoder(automobile, many=True).data
        return Response(data, status=400)
    
    def delete(self, request, pk):
        AutomobileVO.objects.get(pk=pk).delete()
        return Response()



