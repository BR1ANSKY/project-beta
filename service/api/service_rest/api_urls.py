from django.urls import path
from .views import AppointmentViews, TechnicianViews, AutomobileVOViews


urlpatterns = [
    path('appointments/', AppointmentViews.as_view()),
    path('appointments/<int:pk>/', AppointmentViews.as_view()),
    path('technicians/', TechnicianViews.as_view()),
    path('technicians/<int:pk>/', TechnicianViews.as_view()),
    path('automobiles/', AutomobileVOViews.as_view()),
    path('automobiles/<int:pk>/', AutomobileVOViews.as_view()),
]


