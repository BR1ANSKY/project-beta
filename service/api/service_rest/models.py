from django.db import models
from django.utils import timezone


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.last_name}, {self.first_name}"
    

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Appointment(models.Model):
    STATUS_OPTIONS = [
        ('Scheduled', "Scheduled"),
        ('In Progress', 'In Progress'),
        ('Complete', 'Complete')
    ]
    
    status = models.CharField(max_length=20, choices=STATUS_OPTIONS, default='Scheduled')
    date_time = models.DateTimeField()
    reason = models.TextField()
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    
    technician = models.ForeignKey(Technician, related_name="technician", on_delete=models.CASCADE,)
    automobile = models.ForeignKey(AutomobileVO, related_name="automobile", on_delete=models.CASCADE,)  

    def __str__(self):
        return f"{self.customer} - {self.date_time}: {self.status}"
    
    class Meta:
        ordering = ["customer"]
