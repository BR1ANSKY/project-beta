# CarCar

Team:

* Ivan Ibarra - sales
* Brian Cruz - service

## Design
![image info](design.png)
## Service microservice

Explain your models and integration with the inventory
microservice, here.

For services I was asked to build three models. I built models for Technician, Appointments, and AutomobileVO. All microservices were integrated and connected through the AutomobileVO because every car woud have a uniquie vin number. As I setup the models I used the inventory models as a reference to help guide me through the building of the model classes. I used the guidlines provided to us on learn in order to assign each model its attribuites.

For the views and the encoder I was going to use function based views and based of the reference from inventory views it seemed like a lot to write and was hard to organize or look at. I did some research in documentation to see if there was another way to setup views and I found that you could setup class based views. I liked the class views setup better because it was way easier to organize and not so hard to look at compared to defining the function views. I didn't have enough time but I could have made it more DRY and proably defined the request methods once and then had the individual class views inherit those request functions so I didn't have to keep defining them. There was more I could have done with APIViews with the rest framework but didn't get to it. I thought it was cool that APIViews support both JSON and custom data serialization for models. APIViews status codes are also consistent with HTTP by default so instead of getting the normal Django 200 error it would give HTTP errors instead. Overall I think that using the class views was better because I feel like the developer experience is much better than using function based views. Class views allow for the reuse of common logic unlike what I did with my views(Which I would like to change later), encourages DRY code, and better organization. 

## Sales microservice
Four models AutomobileVO, SalesPerson, Customer, and RecordOfSale.
SalesPerson. The fields first name, last name, and the Employee's ID.
For the Customer The fields first/last name, address, and phone number to collect our Customer's contact info.
AutoMobile Two fields of VIN specific to each car in our database and sold. Users will know whether the vehicle has already been listed as sold.
The Record of sales,  price field and, three foreign keys in the form of Automobile, salesperson, and Customer.
## Inregration
 Inventory and Sales domains collaborates with our Service domain.
 Starts at our inventory domain, which keeps track of all the vehicles available for purchase. The sales and service microservice obtain information from the inventory domain, using a poller, to communicate with the inventory domain to keep track of the available vehicles and ensure up-to-date inventory information.

## Starting up
Make sure you have Docker, Git, and Node.js 18.2 or above


1.Fork this repository


2.Clone the forked repository onto your computer:
git clone https://gitlab.com/BR1ANSKY/project-beta

3.Build and run the project with docker using these commands:

docker volume create beta-data
docker-compose build
docker-compose up

4. View the project at local browser at: http://localhost:3000/
## Route Documentation

## Manufacturers:


GET
http://localhost:8100/api/manufacturers/

Create a manufacturer
POST
http://localhost:8100/api/manufacturers/

Get a specific manufacturer
GET
http://localhost:8100/api/manufacturers/id/

Update a specific manufacturer
PUT
http://localhost:8100/api/manufacturers/id/

Delete a specific manufacturer
DELETE
http://localhost:8100/api/manufacturers/id/

Create and update a manufacturer (Send this JSON body):
-You cannot make two manufacturers with the same name

{
  "name": "Chrysler"
}


Creating, getting and updating a single manufacturer return value:

{
	"href": "/api/manufacturers/2/",
	"id": 2,
	"name": "Chrysler"
}


Getting a list of manufacturers return value:

{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "Daimler-Chrysler"
    }
  ]
}



## Vehicle Models:

List vehicle models
GET
http://localhost:8100/api/models/

Create a vehicle model
POST
http://localhost:8100/api/models/

Get a specific vehicle model
GET
http://localhost:8100/api/models/id/

Update a specific vehicle model
PUT
http://localhost:8100/api/models/id/

Delete a specific vehicle model
DELETE
http://localhost:8100/api/models/id/

Create a vehicle model (Send this JSON body):

{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
  "manufacturer_id": 1
}


Update a vehicle model, can take name and/or picture URL:

It is not possible to update a vehicle model's manufacturer


{
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com"
}

Creating and updating a vehicle model return value:

This also returns manufacturer's informations as well


{
  "href": "/api/models/1/",
  "id": 1,
  "name": "Sebring",
  "picture_url": "image.yourpictureurl.com",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "Daimler-Chrysler"
  }
}


Getting a list of vehicle models return value:

{

  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "Sebring",
      "picture_url": "image.yourpictureurl.com",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
      }
    }
  ]
}



## Automobiles:


List automobile
GET
http://localhost:8100/api/automobiles/

Create an automobile
POST
http://localhost:8100/api/automobiles/:vin/

Get a specific automobile
GET
http://localhost:8100/api/models/:vin/

Update a specific automobile
PUT
http://localhost:8100/api/automobiles/:vin/

Delete a specific automobile
DELETE
http://localhost:8100/api/automobiles/:vin/

Create an automobile with its color, year, VIN, and the id of the vehicle model (send this json body)

{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}


Update the color, year, and sold status of an automobile.

{
  "color": "red",
  "year": 2012,
  "sold": true
}


Getting a list of automobiles returns a list of the detail information with the key "autos".

{
	"autos": [
		{
			"href": "/api/automobiles/1C3CC5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1C3CC5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/1/",
					"id": 1,
					"name": "Chrysler"
				}
			},
			"sold": false
		}
	]
}


## Salespeople:
List sales
GET	http://localhost:8090/api/salespeople/

Create a salesperson
POST http://localhost:8090/api/salespeople/

Delete a specific salesperson
DELETE http://localhost:8090/api/salespeople/:id/
Create a sales person by

		{
			"first_name": "John",
			"last_name": "Smith",
			"employee_id": 123,
		},

Getting a list of sales people returns detial information

## Customers:
List customers
GET	http://localhost:8090/api/customers/

Create a customer
POSThttp://localhost:8090/api/customers/

Delete a specific customer
DELETE	http://localhost:8090/api/customers/:id/

## Sales:
GET	http://localhost:8090/api/sales/

Create a sale
POST http://localhost:8090/api/sales/

Delete a sale
DELETE	http://localhost:8090/api/sales/:id
